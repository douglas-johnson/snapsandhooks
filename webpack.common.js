/**
 * Webpack Config
 */

const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const lessPluginGlob = require('less-plugin-glob');
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
	entry: {
		app: './src/react/app.jsx',
		critical: './src/less/critical.less'
	},
	module: {
		rules: [
		{
			test: /\.(js|jsx)$/,
			exclude: /(node_modules)/,
			loader: 'babel-loader',
			options: {
				presets: [
					'@babel/preset-env',
					[
						"@babel/preset-react", {
							"runtime": "automatic"
						}
					]
				]
			}
		},
		/**
		 * Extract Crticial CSS to File
		 */
		{
			test: /\/less\/.+\.less$/,
			use: [
				MiniCssExtractPlugin.loader,
				{
					loader: 'css-loader',
				},
				{
					loader: 'less-loader',
					options: {
						lessOptions: {
							plugins: [
								lessPluginGlob
							],
							paths: [
								path.resolve( __dirname, 'src/less' )
							]
						}
					}
				}	
			]
		},
		/**
		 * Keep Component CSS with Component
		 */
		{
			test: /\/components\/.+\.less$/,
			use: [
				'style-loader',
				'css-loader',
				{
					loader: 'less-loader',
					options: {
						lessOptions: {
							plugins: [
								lessPluginGlob
							],
							paths: [
								path.resolve( __dirname, 'src/less' )
							]
						}
					}
				}
			]
		}
		]
	},
	resolve: {
		extensions: ['*', '.js', '.jsx']
	},
	output : {
		publicPath: '/react/',
		path: path.resolve(__dirname, 'dist/react'),
		chunkFilename: '[name].js'
	},
	optimization: {
		minimize: true,
		minimizer: [
		  new CssMinimizerPlugin(),
		  new TerserPlugin()
		]
	  },
	plugins: [
		new CleanWebpackPlugin(),
		new MiniCssExtractPlugin({
			filename: '[name].css',
			chunkFilename: '[id].css'
		}),
		new MiniCssExtractPlugin()
	]
};
