/**
 * Post Previews
 */

import { useState, useEffect } from 'react';
import { useAsyncValue } from 'react-router-dom';
import PostPreview from './PostPreview';

/**
 * Post Previews
 */
export default function PostPreviews() {

	const response = useAsyncValue();
	const [posts, setPosts] = useState([])

	useEffect(() => {
		(async () => {
			if ( ! response.bodyUsed ) {
				setPosts( await response.json() )
			}
		})();
	}, [response]);

	const previews = posts.map( (post) => {
		return (
			<li key={`post-${post.id}`}>
				<PostPreview post={post} />
			</li>
		)
	} );

	return (
		<nav>
			<menu>
				{ previews }
			</menu>
		</nav>
	);
}
