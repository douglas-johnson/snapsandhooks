/**
 * Shell
 */

import { Outlet, Link, ScrollRestoration } from 'react-router-dom';

/**
 * Shell
 */
export default function Shell() {
	return (
		<>
			<ScrollRestoration />
			<header>
				<div className="container">
					<p><Link to="/">Snaps & Hooks</Link></p>
					<p>The personal portfolio of Douglas Johnson</p>
				</div>
			</header>
			<main>
				<div className="container">
					<Outlet />
				</div>
			</main>
			<footer>
				<div className="container stack-2">
					<h3>Availability</h3>
					<ul>
						<li>I am employed and not generally seeking freelance work.</li>
						<li>I am open to proposals for exceptionally interesting or rewarding long-term projects.</li>
						<li>I am available for one pro-bono project each year for mission driven organizations.</li>
					</ul>
					<h3>Contact</h3>
					<p>Contact me about either of the above at <a href="mailto:hello@douglasjohnson.dev">hello@douglasjohnson.dev</a></p>
					<nav>
						<menu>
							<li><a href="https://github.com/douglas-johnson/">GitHub</a></li>
							<li><a href="https://gitlab.com/douglas-johnson/">GitLab</a></li>
							<li><a href="https://codepen.io/thedougaboveall/">Codepen</a></li>
							<li><a href="https://www.linkedin.com/in/douglasjamesjohnson/">LinkedIn</a></li>
						</menu>
					</nav>
				</div>
			</footer>
		</>
	);
}
