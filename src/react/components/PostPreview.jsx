/**
 * Post Preview
 */
import { Link } from 'react-router-dom';

/**
 * Post Preview
 */
export default function PostPreview( { post } ) {
	return (
		<Link to={`post/${ post.slug }`}>{ post.title.rendered}</Link>
	);
}
