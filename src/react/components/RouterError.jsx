/**
 * Router Error
 */

import { useRouteError } from 'react-router-dom';

/**
 * Router Error
 */
export default function RouterError() {
	const error = useRouteError();
	return (
		<div id="error-page">
			<p>Sorry, an unexpected error has occurred.</p>
			<p>
				<i>{error.statusText || error.message}</i>
			</p>
		</div>
	);
}
