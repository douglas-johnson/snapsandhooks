/**
 * Post
 */

import { useLoaderData } from 'react-router-dom';
import { API_BASE } from '../constants'

/**
 * Post Component
 */
export function Component() {

	const posts = useLoaderData();

	if ( 0 === posts.length ) {
		throw new Response('', {
			status: 404,
			statusText: 'Not Found'
		});
	}

	const post = posts[0];

	return(
		<article className="stack-3">
			<header>
				<h1>{post.title.rendered}</h1>
			</header>
			<div className="content stack-2" dangerouslySetInnerHTML={{ __html: post.content.rendered }} />
		</article>
	)
}

/**
 * Post Loader
 */
export async function loader( { request, params } ) {

	const response = await fetch(
		`${API_BASE}/posts?slug=${params.slug}`,
		{signal: request.signal}
	);

	if ( ! response.ok ) {
		throw response;
	}

	return response;
}
