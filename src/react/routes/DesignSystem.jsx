/**
 * Design System
 */

export function Component() {
	return (
		<article class="content stack-3">
			<h1>Quo ille studio blanditiarum exquisito sublatus inmunemque</h1>
			<h2>Democriti regeret, quos Anaxarcho incitante</h2>
			<h3>Iamque post miserandam deleti Caesaris cladem sonante</h3>
			<h4>Hac enim superabatur difficultate</h4>
			<h5>Sed contra accidentia vir magnanimus stabat immobilis</h5>
			<h6>Inpugnabat autem eum per fictae benignitatis</h6>
			<p>Utcumque potuimus veritatem scrutari, ea quae videre licuit per aetatem, vel perplexe interrogando versatos in medio scire, narravimus ordine casuum exposito diversorum: residua quae secuturus aperiet textus, <small>pro virium captu limatius</small> absolvemus, nihil obtrectatores longi, ut putant, operis formidantes. Tunc enim laudanda est brevitas cum moras rumpens intempestivas nihil subtrahit cognitioni gestorum.</p>
			<h2>Democriti regeret, quos Anaxarcho incitante</h2>
			<p>Nondum apud Noricum exuto penitus Gallo Apodemius quoad vixerat igneus turbarum incentor raptos eius calceos vehens equorum permutatione veloci, ut nimietate cogendi quosdam extingueret, praecursorius index Mediolanum advenit ingressusque regiam ante pedes proiecit Constantii velut spolia regis occisi Parthorum et perlato nuntio repentino, docente rem insperatam et arduam ad sententiam totam facilitate completam, hi qui summam aulam tenebant, omni placendi studio in adulationem ex more conlato virtutem felicitatemque imperatoris extollebant in Caelum, cuius nutu in modum gregariorum militum licet diversis temporibus duo exauctorati sunt principes, Veteranio nimirum et Gallus.</p>
			<blockquote>
				<p>Ut enim subterraneus serpens foramen subsidens occultum adsultu subito singulos transitores observans incessit.</p>
				<cite>Domitius Corbulo</cite>
			</blockquote>
			<ul>
				<li>Indeque ad Iulianum recens perductum calumniarum</li>
				<li>Constantinopolim transeuntem viderat fratrem.</li>
			</ul>
			<ol>
				<li>Indeque ad Iulianum recens perductum calumniarum</li>
				<li>Constantinopolim transeuntem viderat fratrem.</li>
			</ol>
			<p>Nec defuere deinceps ex his emergentia casibus, <a href="#">quae dispiceres secundis</a> avibus contigisse, dum punirentur ex iure, vel tamquam inrita diffluebant et vana. <em>Sed accidebat non numquam</em>, ut opulenti pulsantes praesidia potiorum isdemque tamquam ederae celsis arboribus <strong>adhaerentes absolutionem pretiis mercarentur immensis</strong>: tenues vero, quibus exiguae res erant ad redimendam salutem aut nullae, damnabantur abrupte. Ideoque et veritas mendaciis velabatur et valuere pro veris aliquotiens falsa.</p>
			<p>Perductus est isdem <u>diebus et Gorgonius</u>, cui erat thalami Caesariani cura commissa, cumque eum ausorum fuisse participem concitoremque <code>interdum ex confesso pateret</code>, conspiratione spadonum iustitia concinnatis mendaciis obumbrata, periculo evolutus abscessit.</p>
			<pre>
				<code>
				{
`Haec dum Mediolani aguntur,
	militarium catervae ab oriente perductae sunt Aquileiam cum aulicis pluribus, membris inter catenas
	fluentibus spiritum trahentes exiguum vivendique moras per aerumnas detestati multiplices.`
				}
				</code>
			</pre>
			<figure>
				<img src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%221200%22%20height%3D%22800%22%20viewBox%3D%220%200%201200%20800%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3Crect%20width%3D%221200%22%20height%3D%22800%22%20fill%3D%22%23000000%22%3E%3C%2Frect%3E%3Ctext%20fill%3D%22%23fdfcff%22%20font-family%3D%22sans-serif%22%20x%3D%2250%25%22%20y%3D%2250%25%22%20text-anchor%3D%22middle%22%20font-size%3D%22100%22%3E1200x800%3C%2Ftext%3E%3C%2Fsvg%3E" width="1200" height="800" alt="1200x800 Placeholder Image" />
				<figcaption>
					Arcessebantur enim ministri fuisse Galli.
				</figcaption>
			</figure>
		</article>
	)
}
