/**
 * Home
 */

import { Suspense } from 'react';
import { Await, defer, useLoaderData } from 'react-router-dom';
import PostPreviews from '../components/PostPreviews';
import { API_BASE } from '../constants'

/**
 * Home Component
 */
export function Component() {
	const { posts } = useLoaderData();
	return (
		<article className="stack-2">
			<p>Hello, I'm Douglas Johnson &mdash; Senior Creative Technologist at Thought &amp; Expression Co.</p>
			<p>We publish <a href="https://thoughtcatalog.com">Thought Catalog</a>, <a href="https://creepycatalog.com">Creepy Catalog</a> and <a href="https://collective.world">Collective World</a>. You can find our books and other physical goods on <a href="https://shopcatalog.com">Shop Catalog</a>. We also maintain <a href="https://clearingfarm.com/">Clearing Farm</a>, our retreat center and event venue.</p>
			<h2>Recent Highlights</h2>
			<ul>
				<li>I talked about bit about being a web developer as part of the <a href="https://collectiveworld.substack.com/p/everything-you-need-to-know-about">creative careers feature on Collective World's Substack</a>.</li>
				<li><a href="https://wpvip.com/case-studies/how-thought-catalog-powers-a-network-of-websites/">WordPress VIP interviewed me</a> about the ways Thought Catalog has utilized their platform in my time with company.</li>
				<li>We started rolling out a new brand system. Checkout the new look of the <a href="https://thoughtcatalog.com">Thought Catalog</a> homepage, <a href="https://thoughtcatalog.com/books/">Thought Catalog Books</a> and <a href="https://thoughtcatalog.agency">Thought Catalog Agency</a>.</li>
			</ul>
			<h2>Recent Posts</h2>
			<Suspense fallback={<p>Loading...</p>}>
				<Await resolve={posts}>
					<PostPreviews />
				</Await>
			</Suspense>
		</article>
	);
}

/**
 * Home Loader
 */
export async function loader() {
	return defer({
		posts: fetch( `${API_BASE}/posts`)
	});
}
