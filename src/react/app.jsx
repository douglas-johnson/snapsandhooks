/**
 * App
 * React
 */

// Service Worker
import './ServiceWorkerInit'

// React
import ReactDOM from 'react-dom/client';
import React from 'react';

// Router
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import Shell from './components/Shell';
import RouterError from './components/RouterError';

const router = createBrowserRouter([
	{
		path: '/',
		element: <Shell />,
		errorElement: <RouterError />,
		children: [
			{
				index: true,
				lazy: () => import( /* webpackChunkName: "Home" */ './routes/Home' )
			},
			{
				path: 'post/:slug',
				lazy: () => import( /* webpackChunkName: "Post" */ './routes/Post' )
			},
			{
				path: 'design-system',
				lazy: () => import( /* webpackChunkName: "DesignSystem" */ './routes/DesignSystem' )
			}
		]
	}
]);

(ReactDOM.createRoot( document.getElementById( 'root' ) ) ).render(
	<React.StrictMode>
		<RouterProvider router={router} />
	</React.StrictMode>
);
