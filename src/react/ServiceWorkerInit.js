/**
 * Service Worker Init
 */

if ( supportsServiceWorkers() && true === USE_SERVICE_WORKER ) {
	navigator.serviceWorker.register( '/sw.js' );
}

/**
 * Supports Service Workers
 *
 * @return {Boolean}
 */
function supportsServiceWorkers() {
	return ( 'navigator' in window && 'serviceWorker' in navigator );
}
