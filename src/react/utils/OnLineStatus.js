/**
 * Is Online
 * @return {Boolean}
 */
export function isOnline() {

	if ( ! 'navigator' in window ) {
		return true;
	}

	if ( ! 'onLine' in navigator ) {
		return true;
	}

	return navigator.onLine;

}

/**
 * Get Not Found Message
 * @return {string}
 */
export function getNotFoundMessage() {

	let message = 'Not Found'

	if ( ! isOnline() ) {
		message += ', Or Not Available Offline'
	}

	return message;

}

/**
 * Derive Error Message
 * @param  Error caught
 * @return string
 */
export function deriveErrorMessage( caught ) {

	if ( caught instanceof Error ) {
		return caught.message
	}

	return "Sorry, something's gone wrong.";

}
