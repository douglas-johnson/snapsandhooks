/**
 * Webpack Config
 */

const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const webpack = require('webpack');

module.exports = merge( common, {
	mode: 'production',
	plugins:[
		new webpack.DefinePlugin({
  			"USE_SERVICE_WORKER": JSON.stringify(true)
		})
	]
});
