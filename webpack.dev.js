/**
 * Webpack Config
 */

const path = require('path');
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const webpack = require('webpack');

module.exports = merge( common, {
	mode: 'development',
	devServer: {
		hot: true,
		static: {
			directory: path.join(__dirname, 'dist'),
		},
		port: 8010,
		historyApiFallback: {
      		index: 'index.html'
    	}
	},
	plugins:[
		new webpack.DefinePlugin({
  			"USE_SERVICE_WORKER": JSON.stringify(false)
		})
	]
});
