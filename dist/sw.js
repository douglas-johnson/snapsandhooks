/**
 * Service Worker
 * Reminders:
 * - Updated when the user starts a new browser session.
 */

/**
 * Caches
 */

/**
 * Get Cache Get
 * @param  {Object} c
 * @return {String}
 */
function getCacheKey( c ) {
	return c.name + 'v' + c.version;
}

const cacheContent = {

	name: 'CMSCONTENT',
	version: '2.0.0',
	preCache: [],
	urlTest: function ( urlPath ) {
		return /^\/cms\/wp-json\/wp\/v2\//.test( urlPath );
	}

};

const cacheImages = {

	name: 'IMAGES',
	version: '2.0.0',
	preCache: [
		'/images/icon-48.png',
		'/images/icon-72.png',
		'/images/icon-96.png',
		'/images/icon-144.png',
		'/images/icon-192.png',
		'/images/icon-512.png'
	],
	urlTest: function ( urlPath ) {
		return /^\/images\//.test( urlPath );
	}

};

const cacheReact = {

	name: 'REACT',
	version: '4.0.0',
	preCache: [
		'/react/app.js',
		'/react/critical.css',
		'/react/DesignSystem.js',
		'/react/Home.js',
		'/react/Post.js'
	],
	urlTest: function( urlPath ) {
		return /^\/react\//.test( urlPath )
	}

};

const cacheRoot = {

	name: 'ROOT',
	version: '2.0.0',
	preCache: [
		'/',
		'/favicon.ico'
	],
	urlTest: function( urlPath ) {
		return false;
	}

};

const cachePages = {

	name: 'PAGES',
	version: '2.2.0',
	preCache: [],
	urlTest: function ( urlPath ) {
		return /^\/post\//.test( urlPath )
	}

};

const _caches = [
	cacheContent,
	cacheImages,
	cachePages,
	cacheReact,
	cacheRoot
];

/**
 * Events
 */

self.addEventListener( 'install', onInstall );
self.addEventListener( 'fetch', onFetch );
self.addEventListener( 'activate', onActivate );

/**
 * On Install
 * @param {ExtendableEvent} event
 */
function onInstall( event ) {

	event.waitUntil( precache() );

}

/**
 * Precache
 * @return {Promise}
 */
function precache() {

	return Promise.all( _caches.map( function( _cache ) {

		return caches.open( getCacheKey( _cache ) ).then( function( cache ) {

			// do we need to bind this?
	    	return cache.addAll( _cache.preCache );

		});

	}));

}

/**
 * On Fetch
 * @param {ExtendableEvent} event
 */
function onFetch( event ) {

	const url = new URL( event.request.url );
	const path = url.pathname;

	if ( cacheReact.urlTest( path ) ) {
		return cacheFirst( event, cacheReact )
	}

	if ( cacheContent.urlTest( path ) ) {
		return networkFirst( event, cacheContent )
	}

	if ( cacheImages.urlTest( path ) ) {
		return cacheFirst( event, cacheImages )
	}

	if ( cachePages.urlTest( path ) ) {
		return handlePageRequest( event )
	}

	if ( -1 !== cacheRoot.preCache.indexOf( path ) ) {
		return cacheFirst( event, cacheRoot )
	}

	return false;

}

/**
 * Cache First Response
 * @param {ExtenableEvent} event
 * @param {Object} locally scoped cache object, not cache API
 */
function cacheFirst( event, _cache ) {

	event.respondWith(
	  caches.open( getCacheKey( _cache ) ).then(function(cache) {
	    return cache.match(event.request).then(function (response) {
	      return response || fetch(event.request).then(function(response) {
	        cache.put(event.request, response.clone());
	        return response;
	      });
	    });
	  })
	);

}

/**
 * Network First Response
 * Try to get the resource from the network.
 * Respond with cache if not found.
 * @param {ExtenableEvent} event
 * @param {Object} locally scoped cache object, not cache API
 */
function networkFirst( event, _cache ) {

	event.respondWith(
		fetch( event.request ).then(function(response){
			return caches.open( getCacheKey( _cache ) ).then(function(cache) {
				cache.put(event.request, response.clone());
				return response;
			});
		}).catch(function(err){
			return caches.open( getCacheKey( _cache ) ).then(function(cache) {
				return cache.match(event.request).then(function (response) {
					if ( response) {
						return response;
					}
					return new Response( '[]', {
						status: 404,
						statusText: "Service Worker: Not Found in Cache"
					});
				});
			})
		})
	);
}

/**
 * Handle
 * Always return index where SPA is served
 * @param {ExtenableEvent} event
 */
function handlePageRequest( event ) {

	event.respondWith(
		caches.open( getCacheKey( cacheRoot ) ).then( function( cache ) {
			return cache.match( '/' ).then( function( response ) {
				return response;
			});
		} )
	);

}

/**
 * On Activate
 * Delete Stale Caches
 * @param {ExtenableEvent} event
 */
function onActivate( event ) {

	const currentCaches = _caches.map( function( _cache ) {
		return getCacheKey( _cache );
	} );

	event.waitUntil( caches.keys().then( function( cacheNames ) {
		return Promise.all(
			cacheNames.map( function( cacheName ) {
				if ( -1 === currentCaches.indexOf( cacheName ) ) {
					return caches.delete( cacheName );
				}
			}));
		}
	));
}
